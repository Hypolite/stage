Module VREP


This page has for purpose the understanding of the _VREP.jl_ code. This code is in [Julia](#Julia-1) and may be used on the [V-REP simulator](#VREP-1).

# Contents

```@contents
Pages = ["index.md"]
Depth = 3
```
--------------------
# VREP

![logo V-REP](logo_vrep.png)

[**V-REP**](http://www.coppeliarobotics.com/index.html) is a robot simulator based on a distributed control architecture. It means that you can control each object of your simulation independently using your code.

Using this fact, V-REP is an ideal solution for multi-robot applications. It can be used with a lot of different languages and is compatible with all OS.

V-REP also possess a lot of good features that you can just check [here](http://www.coppeliarobotics.com/features.html).

You can download V-REP [here](http://www.coppeliarobotics.com/downloads.html).

In this manual, we will consider that we are working with the **Jaco arm**.

![Jaco arm](jaco.png)

# Julia

![logo Julia](Logo_julia.png)

[**Julia**](https://julialang.org/) is a new language that looks like MatLab or Python. It was designed for high performances. This language may be unintuitive at first but once you get through its [documentation](https://docs.julialang.org/en/v1/) you will understand that it is a rich dynamic language.

Julia is an open source which means it is free for everyone to use. All of its source code is publicly viewable on GitHub.

In this manual, we are going to show you the functions necessary in order to simulate our robotic arm. Those functions are in Julia but they will regularly call V-REP functions using *ccall*. *remoteApi* is used to allow the communication between V-REP and un external application.

You'll find at the end of the manual an explanation of each used V-REP functions but you can always find more information [on this site](http://www.coppeliarobotics.com/helpFiles/en/remoteApiFunctions.htm).


# Manual

## Functions

### startsimulation

```@docs
startsimulation(simx_opmode)
```

Parameters :
* [simx_opmode](#simx_opmode-1)

V-REP functions :
* [simxStart](#simxStart-1)
* [simxStartSimulation](#simxStartSimulation-1)

```@example
function startsimulation(simx_opmode)
    clientID=ccall((:simxStart,"remoteApi"),Int32,(Ptr{String},Int32,Bool,Bool,Int32,Int32),pointer("127.0.0.1"),5555,true,true,5000,5);
    simxSynchronous(clientID,true);
    ccall((:simxStartSimulation,"remoteApi"),Int32,(Cint,Cint),clientID,simx_opmode);
    return clientID
end
```


### setjointposition

```@docs
setjointposition(clientID,q,Nbrejoints,simx_opmode,objectname)
```

Parameters :
* [clientID](#clientID-1)
* [q](#q-1)
* [Nbrejoints](#Nbrejoints-1)
* [simx_opmode](#simx_opmode-1)
* [objectname](#objectname-1)

V-REP functions :
* [simxGetObjectHandle](#simxGetObjectHandle-1)
* [simxSetJointTargetPosition](#simxSetJointTargetPosition-1)

```@example
function setjointposition(clientID,q,Nbrejoints,simx_opmode,objectname)
    handle=zeros(Cint,1);
    handles=zeros(Cint,Nbrejoints);
    for i=1:Nbrejoints
        cerror=ccall((:simxGetObjectHandle,"remoteApi"),Cint,(Cint,Cstring,Ptr{Cint},Cint),clientID,objectname[i],pointer(handle),simx_opmode);
        handles[i]=handle[1];
        simxPauseCommunication(clientID,1);
        ccall((:simxSetJointTargetPosition,"remoteApi"),Cint,(Cint,Cint,Cfloat,Cint),clientID,handles[i],q[i],simx_opmode)
        simxPauseCommunication(clientID,0);
    end
    return
end
```


### setjointvelocity

```@docs
setjointvelocity(clientID,qdot,Nbrejoints,simx_opmode,objectname)
```
Parameters :
* [clientID](#clientID-1)
* [qdot](#qdot-1)
* [Nbrejoints](#Nbrejoints-1)
* [simx_opmode](#simx_opmode-1)
* [objectname](#objectname-1)

V-REP functions :
* [simxGetObjectHandle](#simxGetObjectHandle-1)
* [simxSetJointTargetVelocity](#simxSetJointTargetVelocity-1)

```@example
function setjointvelocity(clientID,qdot,Nbrejoints,simx_opmode,objectname)
    handle=zeros(Cint,1);
    handles=zeros(Cint,Nbrejoints);
    for i=1:Nbrejoints
        cerror=ccall((:simxGetObjectHandle,"remoteApi"),Cint,(Cint,Cstring,Ptr{Cint},Cint),clientID,objectname[i],pointer(handle),simx_opmode);
        handles[i]=handle[1];
        simxPauseCommunication(clientID,1);
        ccall((:simxSetJointTargetVelocity,"remoteApi"),Cint,(Cint,Cint,Cfloat,Cint),clientID,handles[i],qdot[i],simx_opmode)
        simxPauseCommunication(clientID,0);
    end
    return
end
```

###  getjointposition

```@docs
getjointposition(clientID,Nbrejoints,simx_opmode,objectname)
```

Parameters :
* [clientID](#clientID-1)
* [Nbrejoints](#Nbrejoints-1)
* [simx_opmode](#simx_opmode-1)
* [objectname](#objectname-1)

V-REP functions :
* [simxGetObjectHandle](#simxGetObjectHandle-1)
* [simxGetJointPosition](#simxGetJointPosition-1)

```@example
function getjointposition(clientID,Nbrejoints,simx_opmode,objectname)
    q=zeros(Cfloat,1);
    qread=zeros(Cfloat,Nbrejoints);
    handle=zeros(Cint,1);
    handles=zeros(Cint,Nbrejoints);
    for i=1:Nbrejoints
        cerror=ccall((:simxGetObjectHandle,"remoteApi"),Cint,(Cint,Cstring,Ptr{Cint},Cint),clientID,objectname[i],pointer(handle),simx_opmode);
        handles[i]=handle[1];
        ccall((:simxGetJointPosition,"remoteApi"),Cint,(Cint,Cint,Ptr{Cfloat},Cint),clientID,handles[i],pointer(q),simx_opmode)
        qread[i]=q[1];
    end
    return qread
end
```

###  simxPauseCommunication

```@docs
simxPauseCommunication(clientID,v)
```

Parameters :
* [clientID](#clientID-1)
* [v](#v-1)

V-REP functions :
* [simxPauseCommunication](#simxPauseCommunication-1)

```@example
function simxPauseCommunication(clientID,v)
    ccall((:simxPauseCommunication,"remoteApi"),Int32,(Cint,Cint),clientID,v)
end
```

### stopsimulation

```@docs
stopsimulation(clientID,simx_opmode)
```

Parameters :
* [clientID](#clientID-1)
* [simx_opmode](#simx_opmode-1)

V-REP functions :
* [simxStopSimulation](#simxStopSimulation-1)
* [simxFinish](#simxFinish-1)

```@example
function stopsimulation(clientID,simx_opmode)
    ccall((:simxStopSimulation,"remoteApi"),Int32,(Cint,Cint),clientID,simx_opmode)
    ccall((:simxFinish,"remoteApi"),Int32,(Cint,Cint),clientID,simx_opmode)
end
```

### simxSynchronous

```@docs
simxSynchronous(clientID,v)
```

Parameters :
* [clientID](#clientID-1)
* [v](#v-1)

V-REP functions :
* [simxSynchronous](#simxSynchronous-1)

```@example
function simxSynchronous(clientID,v)
    ccall((:simxSynchronous,"remoteApi"),Int32,(Cint,Bool),clientID,v)
end
```

### simxSynchronousTrigger

```@docs
simxSynchronousTrigger(clientID,v)
```

Parameters :
* [clientID](#clientID-1)
* [v](#v-1)

V-REP functions :
* [simxSynchronousTrigger](#simxSynchronousTrigger-1)

```@example
function simxSynchronous(clientID,v)
    ccall((:simxSynchronous,"remoteApi"),Int32,(Cint,Bool),clientID,v)
end
```

### controlrobot

```@docs
controlrobot(q,clientID,opmode1,opmode2,N,objectname)
```

Parameters :
* [q](#q-1)
* [clientID](#clientID-1)
* [opmode1](#opmode1-1)
* [opmode2](#opmode2-1)
* [N](#N-1)
* [objectname](#objectname-1)

V-REP functions :
* _None_

```@example
function controlrobot(q,clientID,opmode1,opmode2,N,objectname)
    qread=zeros(Cfloat,N);
    simxSynchronous(clientID,true);
    setjointposition(clientID,q,N,opmode1,objectname);
    qread=getjointposition(clientID,N,opmode2,objectname);
    simxSynchronousTrigger(clientID,0);
    return qread
end
```

## Parameter

### clientID

```
The client ID. it refers to simxStart.
```

### N

```
The number of joints our robot has.
```

### Nbrejoints

```
The number of joints our robot has.
```

### objectname

```
Defines which object we want to use.
```

### opmode1

```
The first operation mode.
```

### opmode2

```
The second operation mode.
```

### q

```
The target position of the joint.
```

### qdot

```
The target velocity of the joint.
```

### simx_opmode

```
Define the operation mode of our simulation.
```

### v

```
Tells if the communication should pause or not.
```

## V-REP Functions

### simxFinish

Ends the communication thread.

[Find more](http://www.coppeliarobotics.com/helpFiles/en/remoteApiFunctions.htm#simxFinish).

### simxGetJointPosition

Retrieves the intrinsec position of a joint.

[Find more](http://www.coppeliarobotics.com/helpFiles/en/remoteApiFunctions.htm#simxGetJointPosition).

### simxGetObjectHandle

Retrieves an object handle based on its name.

[Find more](http://www.coppeliarobotics.com/helpFiles/en/remoteApiFunctions.htm#simxGetObjectHandle).

### simxPauseCommunication

Temporarily halt the communication thread from sending data. This can be useful if you need to send several values to V-REP that should be received and evaluated at the same time.

[Find more](http://www.coppeliarobotics.com/helpFiles/en/remoteApiFunctions.htm#simxPauseCommunication).

### simxSetJointTargetPosition

Sets the target position of a joint.

[Find more](http://www.coppeliarobotics.com/helpFiles/en/remoteApiFunctions.htm#simxSetJointTargetPosition).

### simxSetJointTargetVelocity

Sets the intrinsic target velocity of a non-spherical joint.

[Find more](http://www.coppeliarobotics.com/helpFiles/en/remoteApiFunctions.htm#simxSetJointTargetVelocity).

### simxStart

Starts a communication thread with V-REP.

[Find more](http://www.coppeliarobotics.com/helpFiles/en/remoteApiFunctions.htm#simxStart).

### simxStartSimulation

Requests a start of a simulation.

[Find more](http://www.coppeliarobotics.com/helpFiles/en/remoteApiFunctions.htm#simxStartSimulation).

### simxStopSimulation

Requests a stop of the running simulation.

[Find more](http://www.coppeliarobotics.com/helpFiles/en/remoteApiFunctions.htm#simxStopSimulation).

### simxSynchronous

This function enables the synchronous operation mode. It means that the client application will be the one to trigger the next simulation step.

[Find more](http://www.coppeliarobotics.com/helpFiles/en/remoteApiFunctions.htm#simxSynchronous).

### simxSynchronousTrigger

Sends a synchronization trigger signal to the server.

[Find more](http://www.coppeliarobotics.com/helpFiles/en/remoteApiFunctions.htm#simxSynchronousTrigger).

# Index

```@index
```
